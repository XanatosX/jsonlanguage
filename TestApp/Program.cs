﻿using jsonLanguage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace TestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo FI = new FileInfo(Assembly.GetExecutingAssembly().Location);
            Language L = new Language(FI.DirectoryName + @"\Language");
            Console.WriteLine("Current: " + L.Name);
            Console.WriteLine("Aviailable: ");
            foreach (string Language in L.AvailableLanguages)
            {
                Console.WriteLine(Language);
            }
            
            Console.WriteLine(L.GetText("Test"));
            Console.WriteLine(L.GetText("Test2"));
            Console.WriteLine(L.GetText(Environment.NewLine));
            L.SetLanguageByName("Englisch");
            Console.WriteLine("Current: " + L.Name);
            Console.WriteLine(L.GetText("Test"));
            Console.WriteLine(L.GetText("Test2"));

            Console.ReadKey();
           
        }
    }
}
