﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace jsonLanguage.Dictonary
{
    public class Jsoninterpret
    {
        public string key { get; set; }
        public string value { get; set; }
    }

    public class LanguageFile
    {
        public string Language { get; set; }
        public string CountryCode { get; set; }
        public List<Jsoninterpret> Jsoninterpret { get; set; }

        private Dictionary<string, string> LookupTable;
        private string _defaultReturn;
        public string Default
        {
            get
            {
                return _defaultReturn;
            }
        }

        public void Initialize()
        {
            LookupTable = new Dictionary<string, string>();
            _defaultReturn = "Can't find key: ";

            foreach (Jsoninterpret item in Jsoninterpret)
            {
                LookupTable.Add(item.key, item.value);
            }
            Jsoninterpret.Clear();
        }

        public string GetText(string key)
        {
            if (LookupTable != null && LookupTable.ContainsKey(key))
            {
                return LookupTable[key];
            }
            return _defaultReturn + key;
        }
    }
}
