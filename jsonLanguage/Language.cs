﻿using jsonLanguage.Dictonary;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using System.Globalization;

namespace jsonLanguage
{
    public class Language
    {
        private string _languageFolder;
        private string _folder;
        private List<LanguageFile> _languages;
        public string[] AvailableLanguages
        {
            get
            {
                return GetLanguageNames();
            }
        }

        private LanguageFile _currentLanguage;
        public string Name
        {
            get
            {
                return _currentLanguage.Language;
            }
        }
        public string CountryCode
        {
            get
            {
                return _currentLanguage.CountryCode;
            }
        }

        public Language(string folder)
        {
            _folder = folder;
            Load();
        }



        private string[] GetLanguageNames()
        {
            string[] availableLanguages = new string[_languages.Count];
            for (int i = 0; i < availableLanguages.Length; i++)
            {
                availableLanguages[i] = _languages[i].Language;
            }
            return availableLanguages;
        }

        private void Load()
        {
            _languages = new List<LanguageFile>();
            if (!Directory.Exists(_folder))
                Directory.CreateDirectory(_folder);
            foreach (string file in Directory.GetFiles(_folder))
            {
                FileInfo currentFile = new FileInfo(file);
                if (currentFile.Extension.ToLower() != ".json")
                    continue;
                try
                {
                    using (StreamReader reader = new StreamReader(currentFile.OpenRead()))
                    {
                        LanguageFile currentLanguage = JsonConvert.DeserializeObject<LanguageFile>(reader.ReadToEnd());
                        if (LanguageAlreadyExists(currentLanguage.CountryCode))
                            continue;
                        currentLanguage.Initialize();
                        _languages.Add(currentLanguage);
                    }

                }
                catch (Exception ex)
                {
                    continue;
                }

            }

            if (!SetLanguageByCountyCode(CultureInfo.CurrentCulture.Name))
            {
                if (_languages.Count > 0)
                    _currentLanguage = _languages[0];
            }
        }

        private bool LanguageAlreadyExists(string CountryCode)
        {
            foreach (LanguageFile language in _languages)
            {
                if (language.CountryCode == CountryCode)
                    return true;
            }
            return false;
        }

        public void ReloadFiles()
        {
            Load();
        }

        public bool SetLanguageByName(string name)
        {
            foreach (LanguageFile language in _languages)
            {
                if (language.Language == name)
                {
                    _currentLanguage = language;
                    return true;
                }
            }
            return false;
        }

        public bool SetLanguageByCountyCode(string countyCode)
        {
            foreach (LanguageFile language in _languages)
            {
                if (language.CountryCode == countyCode)
                {
                    _currentLanguage = language;
                    return true;
                }
            }
            return false;
        }

        public string GetText(string key)
        {
            if (_currentLanguage == null)
                return "No Language loadet!";
            else
                return _currentLanguage.GetText(key);
        }
        
    }
}
